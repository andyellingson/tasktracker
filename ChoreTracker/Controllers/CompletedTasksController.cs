﻿using ChoreTracker.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;

namespace ChoreTracker.Controllers
{
	public class CompletedTasksController : Controller
    {
		private DatabaseContext _dbContext = new DatabaseContext();

		// GET: CompletedTasks
		public ActionResult Index()
        {
			var model = _dbContext.Tasks.Where(x => x.Status == Enums.ChoreStatus.Complete);

			return View(model);
		}

		public ActionResult Cancel(int? id)
		{
			if (id != null)
			{
				var taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);
				taskModel.Status = Enums.ChoreStatus.InProgress;
				

				_dbContext.Tasks.AddOrUpdate(taskModel);
				_dbContext.SaveChanges();
			}

			return RedirectToAction("Index");
		}


		public ActionResult Complete(int? id)
		{
			if (id != null)
			{
				var userId = User.Identity.GetUserId();
				var user = _dbContext.Users.FirstOrDefault(x => x.UserGuid.Equals(userId));

				var taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);

				if (taskModel != null && user != null)
				{
					//this task should probably be deleted here
					taskModel.Status = Enums.ChoreStatus.New;
					taskModel.AssignedAccountId = string.Empty;

					user.Bank += taskModel.Value;
					_dbContext.Users.AddOrUpdate(user);
					_dbContext.Tasks.AddOrUpdate(taskModel);
					_dbContext.SaveChanges();
				}
			}
			return RedirectToAction("Index");
		}

	}
}