﻿using ChoreTracker.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Mvc;

namespace ChoreTracker.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskRepository _taskRepository;
        protected ApplicationDbContext ApplicationDbContext { get; set; }
        protected UserManager<ApplicationUser> UserManager { get; set; }
        public TaskController(ITaskRepository taskRepository)
        {
            this.ApplicationDbContext = new ApplicationDbContext();
            this.UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(this.ApplicationDbContext));
            _taskRepository = taskRepository;
        }

        public ActionResult List()
        {
            return View("TaskView", _taskRepository.GetAllTasks);
        }

        private ActionResult Edit(int id)
        {
            return View();
        }

        public ActionResult Claim(int id)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            



            return View("TaskView", _taskRepository.GetAllTasks);
        }
    }
}