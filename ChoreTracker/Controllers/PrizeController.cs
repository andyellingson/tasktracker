﻿using ChoreTracker.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using ChoreTracker.Extensions;
using System;

namespace ChoreTracker.Controllers
{
    public class PrizeController : Controller
    {
        DatabaseContext _dbContext = new DatabaseContext();

        // GET: api/Prize/5
        public ActionResult Prize()
        {
			//only display prizes that have not been purchased
			//those are displayed on Purchased Prize page
            var model = _dbContext.Prizes.Where(x => String.IsNullOrEmpty(x.OwnerUserGuid));

            return View("PrizeView", model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            PrizeModel prizeModel = new PrizeModel();
            if (id != null)
            {
                prizeModel = _dbContext.Prizes.FirstOrDefault(x => x.Id == id);
            }

            return View("EditPrizeView", prizeModel);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(PrizeModel prizeModel)
        {
            _dbContext.Prizes.AddOrUpdate(prizeModel);
            _dbContext.SaveChanges();
            return RedirectToAction("Prize");
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            PrizeModel prizeModel = new PrizeModel();

            if (id != null)
            {
                prizeModel = _dbContext.Prizes.FirstOrDefault(x => x.Id == id);
                if (prizeModel != null)
                {
                    _dbContext.Prizes.Remove(prizeModel);
                    _dbContext.SaveChanges();
                }
            }

            return RedirectToAction("Prize");
        }

        [Authorize(Roles = "admin, user")]
        public ActionResult BuyPrize(int? id)
        {
            PrizeModel prizeModel = new PrizeModel();
            var userId = User.Identity.GetUserId();

            //get user infor to check bank balance
            var user = _dbContext.Users.FirstOrDefault(x => x.UserGuid == userId);

            if (user != null && id != null)
            {
                prizeModel = _dbContext.Prizes.FirstOrDefault(x => x.Id == id);

	            if (prizeModel != null && user.Bank >= prizeModel.Cost)
                {

                    //decrement quantity
                    prizeModel.Quantity--;

                    //make sure this isn't the last one
                    if (prizeModel.Quantity > 0)
                    {
                        _dbContext.Prizes.AddOrUpdate(prizeModel);
                    }
                    else
                    {
                        //the last one was sold removing prize
                        _dbContext.Prizes.Remove(prizeModel);
                    }

                    //deduct cost from users account
                    user.Bank -= prizeModel.Cost;

					//clone prize for viewing on PurchasedPrizes Page
					var clonedPrize = prizeModel.Clone<PrizeModel>();
					clonedPrize.Quantity = 1;
					clonedPrize.Id = 0;
					clonedPrize.OwnerUserGuid = user.UserGuid;

					//add purchased prize to databse	
					_dbContext.Prizes.Add(clonedPrize);

					//update Prize record info
					_dbContext.Users.AddOrUpdate(user);
					_dbContext.SaveChanges();
                }
            }

            //refresh the page
            return RedirectToAction("Prize");
        }
    }
}
