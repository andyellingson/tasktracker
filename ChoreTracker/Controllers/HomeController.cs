﻿using ChoreTracker.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;

namespace ChoreTracker.Controllers
{
    public class HomeController : Controller
    {
        DatabaseContext _dbContext = new DatabaseContext();

        public ActionResult Index()
        {
            var model = _dbContext.Tasks.Where(x => (String.IsNullOrEmpty(x.AssignedAccountId) && x.Status != Enums.ChoreStatus.Complete));

            return View(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Edit(int? id)
        {
            TaskModel taskModel = new TaskModel();
            if (id != null)
            {
                taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);
            }

            return View("EditView", taskModel);
        }

        [HttpPost]
        public ActionResult Edit(TaskModel taskModel)
        {
            _dbContext.Tasks.AddOrUpdate(taskModel);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            TaskModel taskModel = new TaskModel();

            if (id != null)
            {
                taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);
                if (taskModel != null)
                {
                    _dbContext.Tasks.Remove(taskModel);
                    _dbContext.SaveChanges();
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult Claim(int? id)
        {
            TaskModel taskModel = new TaskModel();
            if (id != null)
            {
                taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);

                taskModel.AssignedAccountId = User.Identity.GetUserId();

                _dbContext.Tasks.AddOrUpdate(taskModel);
                _dbContext.SaveChanges();
            }
                        
            return RedirectToAction("Index");
        }
    }
}