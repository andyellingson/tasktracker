﻿using ChoreTracker.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;

namespace ChoreTracker.Controllers
{
	//should be renamed to something like MyTasksController??
	public class MyTasksController : Controller
    {
        private DatabaseContext _dbContext = new DatabaseContext();

        // GET: AccountView
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();

            var model = _dbContext.Tasks.Where(x => x.AssignedAccountId.Equals(userId) && (x.Status == Enums.ChoreStatus.New || x.Status == Enums.ChoreStatus.InProgress)).ToList<TaskModel>();

            return View("MyTasksView", model);
        }

		public ActionResult Start(int id)
		{
			var taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);

			if (taskModel != null)
			{
				taskModel.Status = Enums.ChoreStatus.InProgress;
				_dbContext.Tasks.AddOrUpdate(taskModel);
				_dbContext.SaveChanges();
			}

			return RedirectToAction("Index");
		}


		public ActionResult Cancel(int id)
        {
            var taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);

            if (taskModel != null)
            {
				taskModel.Status = Enums.ChoreStatus.New;
                taskModel.AssignedAccountId = string.Empty;
                _dbContext.Tasks.AddOrUpdate(taskModel);
                _dbContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Complete(int id)
        {
			var taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);

            if (taskModel != null)
            {
                taskModel.Status = Enums.ChoreStatus.Complete;
                _dbContext.Tasks.AddOrUpdate(taskModel);
                _dbContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}