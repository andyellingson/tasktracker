﻿using ChoreTracker.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChoreTracker.Controllers
{
    public class PrizePurchasesController : Controller
    {
		DatabaseContext _dbContext = new DatabaseContext();

		// GET: api/Prize/5
		public ActionResult Index()
		{
			//only display prizes that have been purchased
			//The other prizes are displayed on the Prize page
			var model = _dbContext.Prizes.Where(x => !String.IsNullOrEmpty(x.OwnerUserGuid));

			return View("Index", model);
		}

		[Authorize(Roles = "admin")]
		public ActionResult Complete(int id)
		{
			var model = _dbContext.Prizes.FirstOrDefault(x => x.Id == id);

			if (model != null)
			{
				//delete from database
				_dbContext.Prizes.Remove(model);
				_dbContext.SaveChanges();
			}

			return RedirectToAction("Index");
		}		
	}
}
