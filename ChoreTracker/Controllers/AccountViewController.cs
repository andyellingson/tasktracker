﻿using ChoreTracker.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;

namespace ChoreTracker.Controllers
{
    //should be renamed to something like MyTasksController??
    public class AccountViewController : Controller
    {
        DatabaseContext _dbContext = new DatabaseContext();
        protected ApplicationDbContext _applicationDbContext { get; set; }
        protected UserManager<ApplicationUser> UserManager { get; set; }

        public AccountViewController()
        {
            this._applicationDbContext = new ApplicationDbContext();
            this.UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(this._applicationDbContext));
        }

        // GET: AccountView
        public ActionResult Index()
        {
            var model = new List<TaskModel>();
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                model = _dbContext.Tasks.Where(x => x.AssignedAccountId.Equals(user.Id)).ToList<TaskModel>();
            }
            return View("AccountView", model);
        }

        public ActionResult Cancel(int id)
        {
            var taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);

            if(taskModel != null)
            {
                taskModel.AssignedAccountId = string.Empty;
                _dbContext.Tasks.AddOrUpdate(taskModel);
                _dbContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Complete(int id)
        {
            var taskModel = _dbContext.Tasks.FirstOrDefault(x => x.Id == id);

            var user = UserManager.FindById(User.Identity.GetUserId());

            if (taskModel != null)
            {
                taskModel.Status = Enums.ChoreStatus.Complete;
                taskModel.AssignedAccountId = string.Empty;
                user.BankAccount += taskModel.Value;
                this._applicationDbContext.Users.AddOrUpdate(user);
                this._applicationDbContext.SaveChanges();
                _dbContext.Tasks.AddOrUpdate(taskModel);
                _dbContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}