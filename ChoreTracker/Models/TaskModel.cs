﻿using System;
using System.Collections.Generic;
using System.Linq;
using static ChoreTracker.Models.Enums;

namespace ChoreTracker.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public ChoreStatus Status { get; set; } = ChoreStatus.New;
        public string Name { get; set; }
        public string Description { get; set; }
        public int Value { get; set; } = 0;
        public string AssignedAccountId { get; set; }
        public DateTime DueDate { get; set; } = DateTime.Now;
    }
}
