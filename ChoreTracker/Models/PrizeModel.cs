﻿using System;
using static ChoreTracker.Models.Enums;

namespace ChoreTracker.Models
{
	public class PrizeModel
    {
        public int Id { get; set; }
        public int Cost { get; set; }
        public int Quantity { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime ExpirationDate { get; set; } = DateTime.Now.AddDays(14);
        public PrizeCategory Category { get; set; }
		public string OwnerUserGuid { get; set; }
	}


}