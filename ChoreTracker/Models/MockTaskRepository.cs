﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChoreTracker.Models
{
    public class MockTaskRepository : ITaskRepository
    {
        public IEnumerable<TaskModel> GetAllTasks
        {
            get
            {
                return new List<TaskModel>()
                {
                    new TaskModel(){Id=1, Name="Wash Car", Description="Clean Outside of Car", Value=3 },
                    new TaskModel(){Id=2, Name="Mow Lawn", Description="Mow Entire Lawn", Value=2 },
                    new TaskModel(){Id=3, Name="Clean Room", Description="Pick up room",Value=2 },
                    new TaskModel(){Id=4, Name="Take Out Garbage", Description="Take out garbage",Value=3 }
                };

            }
        }

        public TaskModel GetTaskById(int taskId)
        {
            return new TaskModel();
        }
    }
}
