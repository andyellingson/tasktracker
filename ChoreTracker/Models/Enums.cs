﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChoreTracker.Models
{
    public class Enums
    {
		public enum PrizeCategory
		{
			[Display(Name = "Candy")]
			Candy,
			[Display(Name = "Toy")]
			Toy,
			[Display(Name = "Privilege")]
			Privilege
		}
		public enum ChoreStatus
        {
            [Display(Name = "New")]
            New,
            [Display(Name = "In Progress")]
            InProgress,
            [Display(Name = "Complete")]
            Complete,
            [Display(Name = "Expired")]
            Expired
        }
    }
}