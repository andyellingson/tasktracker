﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChoreTracker.Models
{
    public class DatabaseContext : DbContext
    {
        //public DatabaseContext() : base("DBConnection") { }
        public DbSet<TaskModel> Tasks { get; set; }
        public DbSet<PrizeModel> Prizes { get; set; }
        public DbSet<UserModel> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DatabaseContext>(null);
            base.OnModelCreating(modelBuilder);
        }

    }
}